<?php


Route::get('/', function() {
   return Redirect::to('/movies');
});


Route::get('/login', 'AuthController@login');
Route::post('/login', 'AuthController@loginPost');


Route::resource('genres', 'GenresController');

Route::resource('movies', 'MoviesController');
Route::get('movies/{id}/actors', 'MoviesController@actors');

Route::resource('actors', 'ActorsController');
Route::get('actors/{id}/movies', 'ActorsController@movies');

Route::resource('users', 'UsersController');

Route::resource('directors', 'DirectorsController');
