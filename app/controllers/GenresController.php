<?php

class GenresController extends BaseController {

	/**
	 * Genre Repository
	 *
	 * @var Genre
	 */
	protected $genre;

	public function __construct(Genre $genre)
	{
		$this->genre = $genre;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$genres = $this->genre->all();

		return View::make('genres.index', compact('genres'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('genres.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Genre::$rules);

		if ($validation->passes())
		{
			$this->genre->create($input);

			return Redirect::route('genres.index');
		}

		return Redirect::route('genres.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$genre = $this->genre->findOrFail($id);

		return View::make('genres.show', compact('genre'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$genre = $this->genre->find($id);

		if (is_null($genre))
		{
			return Redirect::route('genres.index');
		}

		return View::make('genres.edit', compact('genre'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Genre::$rules);

		if ($validation->passes())
		{
			$genre = $this->genre->find($id);
			$genre->update($input);

			return Redirect::route('genres.show', $id);
		}

		return Redirect::route('genres.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->genre->find($id)->delete();

		return Redirect::route('genres.index');
	}

}
