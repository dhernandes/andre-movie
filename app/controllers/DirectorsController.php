<?php

class DirectorsController extends BaseController {

	/**
	 * Director Repository
	 *
	 * @var Director
	 */
	protected $director;

	public function __construct(Director $director)
	{
		$this->director = $director;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$directors = $this->director->all();

		return View::make('directors.index', compact('directors'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('directors.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Director::$rules);

		if ($validation->passes())
		{
			$this->director->create($input);

			return Redirect::route('directors.index');
		}

		return Redirect::route('directors.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$director = $this->director->findOrFail($id);

		return View::make('directors.show', compact('director'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$director = $this->director->find($id);

		if (is_null($director))
		{
			return Redirect::route('directors.index');
		}

		return View::make('directors.edit', compact('director'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Director::$rules);

		if ($validation->passes())
		{
			$director = $this->director->find($id);
			$director->update($input);

			return Redirect::route('directors.show', $id);
		}

		return Redirect::route('directors.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->director->find($id)->delete();

		return Redirect::route('directors.index');
	}

}
