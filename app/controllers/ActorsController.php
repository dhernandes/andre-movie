<?php

class ActorsController extends BaseController {

	/**
	 * Actor Repository
	 *
	 * @var Actor
	 */
	protected $actor;

	public function __construct(Actor $actor)
	{
		$this->actor = $actor;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$actors = $this->actor->all();

		return View::make('actors.index', compact('actors'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('actors.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Actor::$rules);

		if ($validation->passes())
		{
			$this->actor->create($input);

			return Redirect::route('actors.index');
		}

		return Redirect::route('actors.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$actor = $this->actor->findOrFail($id);

		return View::make('actors.show', compact('actor'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$actor = $this->actor->find($id);

		if (is_null($actor))
		{
			return Redirect::route('actors.index');
		}

		return View::make('actors.edit', compact('actor'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Actor::$rules);

		if ($validation->passes())
		{
			$actor = $this->actor->find($id);
			$actor->update($input);

			return Redirect::route('actors.show', $id);
		}

		return Redirect::route('actors.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->actor->find($id)->delete();

		return Redirect::route('actors.index');
	}

}
