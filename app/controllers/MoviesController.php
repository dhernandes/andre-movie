<?php

class MoviesController extends BaseController {

	/**
	 * Movie Repository
	 *
	 * @var Movie
	 */
	protected $movie;

	public function __construct(Movie $movie)
	{
		$this->movie = $movie;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$movies = $this->movie->all();

		return View::make('movies.index', compact('movies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('movies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Movie::$rules);

		if ($validation->passes())
		{
			$this->movie->create($input);

			return Redirect::route('movies.index');
		}

		return Redirect::route('movies.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$movie = $this->movie->findOrFail($id);

		return View::make('movies.show', compact('movie'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$movie = $this->movie->find($id);

		if (is_null($movie))
		{
			return Redirect::route('movies.index');
		}

		return View::make('movies.edit', compact('movie'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Movie::$rules);

		if ($validation->passes())
		{
			$movie = $this->movie->find($id);
			$movie->update($input);

			return Redirect::route('movies.show', $id);
		}

		return Redirect::route('movies.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->movie->find($id)->delete();

		return Redirect::route('movies.index');
	}

}
