<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('GenresTableSeeder');
		$this->call('MoviesTableSeeder');
		$this->call('ActorsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('DirectorsTableSeeder');
	}

}