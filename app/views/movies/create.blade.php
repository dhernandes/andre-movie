@extends('layouts.scaffold')

@section('main')

<h1>Create Movie</h1>

{{ Form::open(array('route' => 'movies.store')) }}
	<ul>
        <li>
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title') }}
        </li>

        <li>
            {{ Form::label('director_id', 'Director:') }}
            {{ Form::select('director_id', Director::all()->lists('name', 'id')) }}
        </li>

        <li>
            {{ Form::label('genre_id', 'Genre:') }}
            {{ Form::select('genre_id', Genre::all()->lists('genre', 'id')) }}
        </li>

        <li>
            {{ Form::label('actor_id', 'Main Actor:') }}
            {{ Form::select('actor_id', Actor::all()->lists('name', 'id')) }}
        </li>

        <li>
            {{ Form::label('release', 'Release Date:') }}
            {{ Form::text('release') }}
        </li>

        <li>
            {{ Form::label('synopsis', 'Synopsis:') }}
            {{ Form::textarea('synopsis') }}
        </li>

        <li>
            {{ Form::label('poster_url', 'Poster Link:') }}
            {{ Form::text('poster_url') }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


