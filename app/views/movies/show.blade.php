@extends('layouts.scaffold')

@section('main')

<h1>Show Movie</h1>

<p>{{ link_to_route('movies.index', 'Return to all movies') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			    <th>Title</th>
				<th>Director</th>
				<th>Genre</th>
				<th>Main Actor</th>
				<th>Release Date</th>
				<th>Synopsis</th>
				<th>Poster</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $movie->title }}}</td>
            <td>
                @if($movie->director)
                {{ link_to_route('directors.show', $movie->director->name, array($movie->director->id)) }}
                @endif
            </td>
            <td>
                @if($movie->genre)
                {{ link_to_route('genres.show', $movie->genre->genre, array($movie->genre->id)) }}
                @endif
            </td>
            <td>
                @if($movie->actor)
                {{ link_to_route('actors.show', $movie->actor->name, array($movie->actor->id)) }}
                @endif
            </td>
            <td>{{{ $movie->release }}}</td>
			<td>{{{ $movie->synopsis }}}</td>
			<td><img width="200" src="{{{ $movie->poster_url }}}"></td>
            <td>{{ link_to_route('movies.edit', 'Edit', array($movie->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('movies.destroy', $movie->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
