@extends('layouts.scaffold')

@section('main')

<h1>Show Director</h1>

<p>{{ link_to_route('directors.index', 'Return to all directors') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
				<th>Country</th>
            <th>Movies</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $director->name }}}</td>
			<td>{{{ $director->country }}}</td>
            <td>
                <ul>
                    @foreach($director->movies as $movie)
                    <li>{{ link_to_route('movies.show', $movie->title, array($movie->id)) }}</li>
                    @endforeach
                    <ul>
            </td>
            <td>{{ link_to_route('directors.edit', 'Edit', array($director->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('directors.destroy', $director->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
