@extends('layouts.scaffold')

@section('main')

<h1>Show Genre</h1>

<p>{{ link_to_route('genres.index', 'Return to all genres') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Genre</th>
            <th>Movies</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $genre->genre }}}</td>
            <td>
                <ul>
                    @foreach($genre->movies as $movie)
                    <li>{{ link_to_route('movies.show', $movie->title, array($movie->id)) }}</li>
                    @endforeach
                    <ul>
            </td>
            <td>{{ link_to_route('genres.edit', 'Edit', array($genre->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('genres.destroy', $genre->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
