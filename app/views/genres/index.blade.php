@extends('layouts.scaffold')

@section('main')

<h1>All Genres</h1>

<p>{{ link_to_route('genres.create', 'Add new genre') }}</p>

@if ($genres->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Genre</th>
                <th>Movies</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($genres as $genre)
				<tr>
					<td>{{{ $genre->genre }}}</td>
                    <td>
                        <ul>
                            @foreach($genre->movies as $movie)
                            <li>{{ link_to_route('movies.show', $movie->title, array($movie->id)) }}</li>
                            @endforeach
                            <ul>
                    </td>
                    <td>{{ link_to_route('genres.edit', 'Edit', array($genre->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('genres.destroy', $genre->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no genres
@endif

@stop
