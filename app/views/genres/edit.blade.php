@extends('layouts.scaffold')

@section('main')

<h1>Edit Genre</h1>
{{ Form::model($genre, array('method' => 'PATCH', 'route' => array('genres.update', $genre->id))) }}
	<ul>
        <li>
            {{ Form::label('genre', 'Genre:') }}
            {{ Form::text('genre') }}
        </li>

		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
			{{ link_to_route('genres.show', 'Cancel', $genre->id, array('class' => 'btn')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
