<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
		<style>
			table form { margin-bottom: 0; }
			form ul { margin-left: 0; list-style: none; }
			.error { color: red; font-style: italic; }
			body { padding-top: 20px; }
		</style>
	</head>

	<body>

		<div class="container">

            <div class="navbar">
                <div class="navbar-inner">
                    <a class="brand" href="/movies">Andre's Movie Database</a>
                    <ul class="nav">
                        <li><a href="/movies">Movies</a></li>
                        <li><a href="/genres">Genres</a></li>
                        <li><a href="/directors">Directors</a></li>
                        <li><a href="/actors">Actors</a></li>
                    </ul>
                </div>
            </div>

			@if (Session::has('message'))
				<div class="flash alert">
					<p>{{ Session::get('message') }}</p>
				</div>
			@endif

			@yield('main')
		</div>

	</body>

</html>