@extends('layouts.scaffold')

@section('main')

<h1>Show Actor</h1>

<p>{{ link_to_route('actors.index', 'Return to all actors') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
				<th>Birth</th>
				<th>Country</th>
                <th>Movies</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $actor->name }}}</td>
					<td>{{{ $actor->birth }}}</td>
					<td>{{{ $actor->country }}}</td>
                    <td>
                        <ul>
                            @foreach($actor->movies as $movie)
                            <li>{{ link_to_route('movies.show', $movie->title, array($movie->id)) }}</li>
                            @endforeach
                        <ul>
                    </td>

            <td>{{ link_to_route('actors.edit', 'Edit Actor', array($actor->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('actors.destroy', $actor->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
