@extends('layouts.scaffold')

@section('main')

<h1>All Actors</h1>

<p>{{ link_to_route('actors.create', 'Add new actor') }}</p>

@if ($actors->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Birth</th>
				<th>Country</th>
                <th>Movies</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($actors as $actor)
				<tr>
					<td>{{{ $actor->name }}}</td>
					<td>{{{ $actor->birth }}}</td>
					<td>{{{ $actor->country }}}</td>
                    <td>
                        <ul>
                            @foreach($actor->movies as $movie)
                            <li>{{ link_to_route('movies.show', $movie->title, array($movie->id)) }}</li>
                            @endforeach
                            <ul>
                    </td>
                    <td>{{ link_to_route('actors.edit', 'Edit', array($actor->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('actors.destroy', $actor->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no actors
@endif

@stop
