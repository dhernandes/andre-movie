<?php

class Movie extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'title' => 'required',
		'director_id' => 'required',
		'genre_id' => 'required',
		'actor_id' => 'required',
		'release' => 'required',
		'synopsis' => 'required',
		'poster_url' => 'required'
	);

    public function genre()
    {
        return $this->belongsTo('Genre');
    }

    public function actor()
    {
        return $this->belongsTo('Actor');
    }

    public function director()
    {
        return $this->belongsTo('Director');
    }
}
