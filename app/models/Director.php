<?php

class Director extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'country' => 'required'
	);

    public function movies()
    {
        return $this->hasMany('Movie');
    }
}
