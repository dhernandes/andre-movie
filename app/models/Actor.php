<?php

class Actor extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'birth' => 'required',
		'country' => 'required'
	);

    public function movies()
    {
        return $this->hasMany('Movie');
    }

}
