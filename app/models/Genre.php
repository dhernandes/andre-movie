<?php

class Genre extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'genre' => 'required'
	);

    public function movies()
    {
        return $this->hasMany('Movie');
    }
}
